$(function(){
    console.log();
    $('body').css({
        paddingTop: $('.main-header .navbar').height()
    });
    
});

var swiper = new Swiper('.swipe_banner', {
    slidesPerView: 'auto',
    spaceBetween: 30,
    loop:true,
    autoplay: { delay: 900, },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
});
  
  
var swiper = new Swiper('.swiper-testimonials', {
    slidesPerView: 'auto',
      centeredSlides: true,
      spaceBetween: 30,
      loop:true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});